// +build ignore

package lex

import (
	"strings"
)

//go:generate ragel -Z parse.rl
//go:generate gofmt -s -w parse.go

%%{
	# This is a ragel file. It's here as an experiment.
	# Here's an invocation to view a pretty graph of the lexer:
	# 	ragel -Z -V -p parse.rl | dot -Tpng -o png && open png
	machine sysmk;
	write data;

	access l.;
	variable p    l.p;
	variable cs   l.cs;
	variable pe   l.pe;
	variable eof   l.eof;

	# it seems like we should be able to make this more concise?
	action mark {
		l.q = append(l.q, Tok{})
		r = &l.q[idx]
		idx++
		l.ts = fpc
	}
	action Type {
		r.Type = Type
		r.Value = string(l.data[l.ts:fpc])
	}
	action ID {
		r.Type = ID
		r.Value = string(l.data[l.ts:fpc])
	}
	action Args {
		r.Type = Args
		r.Value = string(l.data[l.ts:fpc])
	}
	action List {
		r.Type = List
		r.Value = string(l.data[l.ts:fpc])
	}
	action Script {
		l.ts++ // skip the tab
		r.Type = Script
		r.Value = strings.Replace(string(l.data[l.ts:fpc]), "\n\t", "\n", -1)
	}
	action Setup {
		r.Type = Setup
		r.Value = string(l.data[l.ts:fpc])
	}
	action Directive {
		r.Type = Directive
		r.Value = string(l.data[l.ts:fpc])
	}
	action Attrs {
		r.Type = Attrs
		r.Value = string(l.data[l.ts:fpc])
	}

	ws = [\t ];
	id = alpha . (alnum | (punct -- [:,]))*;
	type = (upper . alpha*);
	args = (id (',' ws*)?)+;
	list = (id ws*)+;

	target = (type >mark %Type) '!' id >mark %ID (',' ws* args >mark %Args)? ':' (alpha+ >mark %Attrs ':')? ws* (list >mark %List)? '\n';
	script = ('\t' [^\n]* '\n')+ >mark %Script;
	block = target script? '\n';

	ignore = ('#' [^\n]*)? '\n';

	directive = '=' ws* id>mark  [^\n]*%Directive '\n';
	setup = '-' ws* id>mark  [^\n]*%Setup '\n';

	main := ( ignore | directive | setup | block )*;

}%%
// BUG(hank) This doesn't handle eof correctly right now. Rule blocks *must* have a trailing newline.

// BUG(hank) This doesn't handle streaming right now. The entire file must be loaded into memory.

func New(b []byte) *Lexer {
	l := &Lexer{
		data: b,
		pe: len(b),
		eof: len(b),
	}
	%% write init;
	return l
}

func (l *Lexer) Parse() *Tok {
	var r *Tok
	if len(l.q) > 0 {
		r, l.q = &l.q[0], l.q[1:]
		return r
	}
	idx := 0

	for ;l.pe != l.p; {
%% write exec;
	}

	r, l.q = &l.q[0], l.q[1:]
	return r
}
