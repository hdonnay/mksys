package lex

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
	"unicode"
)

type Kind int

const (
	Illegal Kind = iota
	Error
	EOF

	Directive
	Setup
	Type
	Invocation
	Attribute
	Prerequisite
	Script
)

// ErrInvalidLexer is returned when the lexer is in an invalid state
var ErrInvalidLexer = fmt.Errorf("lexer in invalid state")

type Token struct {
	Kind  Kind
	Value string
}

type Lexer struct {
	r     *bufio.Reader
	q     chan *Token
	err   error
	state stateFn
}

func New(r io.Reader) *Lexer {
	return &Lexer{
		r:     bufio.NewReader(r),
		q:     make(chan *Token, 5),
		state: startState,
	}
}

func (l *Lexer) peek() rune {
	// This just errors real good.
	ch, _, _ := l.r.ReadRune()
	l.r.UnreadRune()
	return ch
}

func (l *Lexer) emit(k Kind, s string) {
	l.q <- &Token{Kind: k, Value: s}
}

func (l *Lexer) error(e error) bool {
	if e == io.EOF {
		l.emit(EOF, "")
		return true
	}
	if e != nil {
		l.emit(Error, e.Error())
		return true
	}
	return false
}

func (l *Lexer) Next() (*Token, error) {
	for {
		select {
		case t := <-l.q:
			return t, nil
		default:
			if l.state == nil {
				return nil, ErrInvalidLexer
			}
			l.state = l.state(l)
		}
	}
	panic("?")
}

type stateFn func(*Lexer) stateFn

func startState(l *Lexer) stateFn {
	ch, _, err := l.r.ReadRune()
	if e := l.error(err); e {
		return nil
	}
	// consume all blank lines
	for ; ch == '\n' && err == nil; ch, _, err = l.r.ReadRune() {
	}
	if e := l.error(err); e {
		return nil
	}
	if e := l.error(l.r.UnreadRune()); e {
		return nil
	}
	return lineState
}

func lineState(l *Lexer) stateFn {
	ch := l.peek()
	switch {
	case ch == '#':
		return commentState
	case ch == '=':
		return directiveState
	case ch == '-':
		return setupState
	case unicode.IsUpper(ch):
		return blockState
	}
	return nil
}

func commentState(l *Lexer) stateFn {
	// discard line, return to start
	_, err := l.r.ReadSlice('\n')
	if e := l.error(err); e {
		return nil
	}
	return startState
}

func directiveState(l *Lexer) stateFn {
	s, err := l.r.ReadString('\n')
	if e := l.error(err); e {
		return nil
	}
	s = strings.TrimLeft(s, "= \t")
	s = strings.TrimRight(s, "\n")
	l.emit(Directive, s)
	return startState
}

func setupState(l *Lexer) stateFn {
	s, err := l.r.ReadString('\n')
	if e := l.error(err); e {
		return nil
	}
	s = strings.TrimLeft(s, "- \t")
	s = strings.TrimRight(s, "\n")
	l.emit(Setup, s)
	return startState
}

func blockState(l *Lexer) stateFn {
	s, err := l.r.ReadString('!')
	if e := l.error(err); e {
		return nil
	}
	s = s[:len(s)-1]
	l.emit(Type, s)
	return invokeState
}

func invokeState(l *Lexer) stateFn {
	s, err := l.r.ReadString(':')
	if e := l.error(err); e {
		return nil
	}
	s = s[:len(s)-1]
	l.emit(Invocation, s)

	return attributeState
}

func attributeState(l *Lexer) stateFn {
	s, err := l.r.ReadString(':')
	if e := l.error(err); e {
		return nil
	}
	s = s[:len(s)-1]
	l.emit(Attribute, s)
	return prereqState
}

func prereqState(l *Lexer) stateFn {
	s, err := l.r.ReadString('\n')
	if e := l.error(err); e {
		return nil
	}
	// We're here because of the preceding space, drop it and the trailing
	// newline.
	s = strings.TrimSpace(s)
	l.emit(Prerequisite, s)
	return scriptState
}

func scriptState(l *Lexer) stateFn {
	buf := &bytes.Buffer{}
	for l.peek() == '\t' {
		b, err := l.r.ReadSlice('\n')
		if e := l.error(err); e {
			return nil
		}
		// skip over the leading \t
		_, err = buf.Write(b[1:])
		if e := l.error(err); e {
			return nil
		}
	}
	l.emit(Script, buf.String())

	return startState
}
