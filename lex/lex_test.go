package lex

import (
	"bytes"
	"io/ioutil"
	"testing"
)

var expect = map[string][]*Token{
	"testdata/simple.mks": []*Token{
		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name, arg"},
		{Kind: Attribute, Value: "ATTR"},
		{Kind: Prerequisite, Value: "target0 target1"},
		{Kind: Script, Value: "#!/bin/cat\nscript body\n"},
		{Kind: EOF, Value: ""},
	},
	"testdata/full.mks": []*Token{
		{Kind: Directive, Value: "directive arguments"},
		{Kind: Setup, Value: "setup arguments"},
		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name, arg"},
		{Kind: Attribute, Value: "ATTR"},
		{Kind: Prerequisite, Value: "target0 target1"},
		{Kind: Script, Value: "#!/bin/cat\nscript body\n"},
		{Kind: Setup, Value: "setup arguments"},
		{Kind: EOF, Value: ""},
	},
	"testdata/tricky.mks": []*Token{
		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name, arg"},
		{Kind: Attribute, Value: "ATTR"},
		{Kind: Prerequisite, Value: "target0 target1"},
		{Kind: Script, Value: "#!/bin/cat\nscript body\n"},

		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name, arg"},
		{Kind: Attribute, Value: "ATTR"},
		{Kind: Prerequisite, Value: "target0 target1"},
		{Kind: Script, Value: "#!/bin/cat\nscript body\n"},

		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name"},
		{Kind: Attribute, Value: "ATTR"},
		{Kind: Prerequisite, Value: "target0 target1"},
		{Kind: Script, Value: "#!/bin/cat\nscript body\n"},
		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name"},
		{Kind: Attribute, Value: ""},
		{Kind: Prerequisite, Value: "target0 target1"},
		{Kind: Script, Value: "#!/bin/cat\nscript body\n"},

		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name"},
		{Kind: Attribute, Value: ""},
		{Kind: Prerequisite, Value: ""},
		{Kind: Script, Value: ""},
		{Kind: Type, Value: "Type"},
		{Kind: Invocation, Value: "name"},
		{Kind: Attribute, Value: ""},
		{Kind: Prerequisite, Value: ""},
		{Kind: Script, Value: ""},

		{Kind: EOF, Value: ""},
	},
}

func harness(t *testing.T, name string) {
	chk := expect[name]
	body, err := ioutil.ReadFile(name)
	if err != nil {
		t.Fatal(err)
	}

	l := New(bytes.NewReader(body))
	var tk []*Token
	for r, err := l.Next(); err == nil; r, err = l.Next() {
		tk = append(tk, r)
		if r.Kind == EOF {
			break
		}
	}

	for i := range chk {
		t.Logf("want: %#v\n", chk[i])
		t.Logf("have: %#v\n", tk[i])
		if chk[i].Kind != tk[i].Kind {
			t.Fatalf("token %d: expected %q, got %q\n", i, chk[i].Kind, tk[i].Kind)
		}
		if chk[i].Value != tk[i].Value {
			t.Fatalf("token %d: expected %q, got %q\n", i, chk[i].Value, tk[i].Value)
		}
	}
}

func TestSimple(t *testing.T) {
	file := "testdata/simple.mks"
	t.Logf("using file %q\n", file)
	t.Parallel()
	harness(t, file)
}

func TestFull(t *testing.T) {
	file := "testdata/full.mks"
	t.Logf("using file %q\n", file)
	t.Parallel()
	harness(t, file)
}

func TestTricky(t *testing.T) {
	file := "testdata/tricky.mks"
	t.Logf("using file %q\n", file)
	t.Parallel()
	harness(t, file)
}
